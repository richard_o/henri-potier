var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    uglify          = require('gulp-uglify'),
    watch           = require('gulp-watch'),
    concat          = require('gulp-concat'),
    minifyCSS       = require('gulp-minify-css'),
    del             = require('del');

var sassSources     = 'src/sass/*.scss',
    jsSources       = 'src/js/**/*.js',
    cssPath         = 'assets/css/',
    jsPath          = 'assets/js/scripts';

gulp.task('clean', function (cb) {
  del([jsPath, cssPath], cb);
});

gulp.task('sass', function () {
    gulp.src(sassSources)
        .pipe(sass())
        .pipe(concat('style.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest(cssPath));
});

gulp.task('uglifyJS', function(){
    gulp.src(jsSources)
        .pipe(concat('scripts.js'))
        //.pipe(uglify())
        .pipe(gulp.dest(jsPath));
});

gulp.task('watch', function() {
    gulp.watch(jsSources, ['uglifyJS']);
    gulp.watch(sassSources, ['sass']);
});

gulp.task('default', ['clean', 'sass', 'uglifyJS', 'watch']);