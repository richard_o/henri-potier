// GENERAL COMPONENT
var Content = React.createClass({
  loadJsonFromUrl: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  getInitialState: function() {
    return {data: []};
  },

  componentDidMount: function() {
    this.loadJsonFromUrl();
    setInterval(this.loadJsonFromUrl, this.props.pollInterval);
  },

  handleClick: function(book){

    console.log('YEAH:  ' + book);
    this.setState({book: book});
  },

  render: function() {
    return (
      <div className="Content">
				<CartPart data={this.state.book}/>
        <BookList data={this.state.data} handleClick={this.handleClick} />
      </div>
    );
  }
});

// CART PART COMPONENT
var CartPart = React.createClass({

	render: function() {

var title = this.props.data ? this.props.data.title : 'NO';
		return (

			<div className="CartPart">
				<header>
        {title}
					<div className="nav">
						<div className="nav__title">
							<h1>Video test</h1>
						</div>
						<div className="nav__cart">
							<span className="nav__cart__icon icon-cart"></span>
							<span className="nav__cart__title">Panier</span>
							<span className="nav__cart__nb-item">4</span>
							<span className="nav__cart__pipe">|</span>
							<span className="nav__cart__price">Prix : 45€</span>
						</div>
					</div>
				</header>
			</div>

		);
	}
})

// BOOKLIST PART COMPONENT
var BookList = React.createClass({

  handleClick: function(book){
    this.props.handleClick(book);
  },

	render: function() {

    var handleClick = this.handleClick;
    var bookData = this.props.data.map(function(book){
      return (
			     <Book book={book} key={book.isbn} handleClick={handleClick} />
			);
    })

    return (
      <div className="booklist">
        {bookData}
      </div>
    );
  }
});

// BOOK COMPONENT
var Book = React.createClass({

  onClick: function(book) {
    console.log(book);
    this.props.handleClick(book);
  },

	render: function() {

    var book = this.props.book;

		return (

			<div className="book">
        <img src={book.cover} className="book__thumb" />
        <div className="book__title">{book.title}</div>
        <div className="book__price">{book.price}</div>
        <div className="book__isbn__title">Numéro ISBN</div>
        <div className="book__isbn__code">{book.isbn}</div>
        <div className="book__add-to-cart" onClick={ this.onClick.bind(this, book) } >Ajouter</div>
			</div>
		)
	}
})

ReactDOM.render(
  <Content url="https://dl.dropboxusercontent.com/u/3077895/henri-potier/books.json" pollInterval={1000} />,
  document.getElementById('content')
);
